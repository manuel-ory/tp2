
function isNumSecu(string) {
	var myPattern = '^[12][0-9]{2}(0[1-9]|1[0-2])(2[AB]|[0-9]{2})[0-9]{3}[0-9]{3}([0-9]{2})?$'; 
    var re = new RegExp(myPattern);
    return (string.match(re)!=null)
}                 