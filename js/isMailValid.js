function ismail(string) {
	var myPattern = '^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'; 
    var re = new RegExp(myPattern);
    return (string.match(re)!=null)
}