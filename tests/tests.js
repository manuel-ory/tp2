QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});

QUnit.test("Fontion paire", function( assert ){
	assert.expect(6);
	assert.equal(isOdd(5), false);
	assert.equal(isOdd(18), true);
	assert.equal(isOdd(0), true);
	assert.notEqual(isOdd(-12), false);
	assert.notEqual(isOdd(19), true);
	assert.notEqual(isOdd(-5), true);
	//assert.equal(isOdd(-5), false);
});

QUnit.test("Fonction bonjour", function( assert ){
	assert.expect(6);
	assert.equal(bonjour("Romain"), "Bonjour Romain");
	assert.equal(bonjour("Pierre"), "Bonjour Pierre");
	assert.equal(bonjour("Julie"), "Bonjour Julie");
	assert.notEqual(bonjour("Kevin"), "Bonjour Vincent");
	assert.notEqual(bonjour("Noémie"), "Bonjour Romain");
	assert.notEqual(bonjour("Manon"), "Bonjour Romain");
});

QUnit.test("Fonction validation N° Sécu", function( assert ){
	assert.expect(3);
	assert.equal(isNumSecu("195034218716079"), true);
	assert.equal(isNumSecu("1950342187160"), true);
	assert.equal(isNumSecu("19502187160"), false);
});

QUnit.test("Fonction validation RIB", function( assert ){

	assert.equal(isRIBvalid("12345","12345","12345678910","19"), true);
	assert.equal(isRIBvalid("12345","12345","12345678910"), 19);
	assert.equal(isRIBvalid("35268","12338","11234455554"), 48);
	assert.equal(isRIBvalid("35268","12338","11234455554","48"), true);
	assert.equal(isRIBvalid("35268","12338","1123445555412","48"), false);

});

QUnit.test("Fonction fibonacci", function( assert ){

	assert.equal(fibonacci(1), 1);
	assert.equal(fibonacci(-3),0);
	assert.equal(fibonacci(15),610);
});

QUnit.test("Fonction validation mail", function( assert ){
	assert.expect(3);
	assert.equal(ismail("batman@joker.fr"), true);
	assert.equal(ismail("stranger.things@2.com"), true);
	assert.equal(ismail("19502187160"), false);
});