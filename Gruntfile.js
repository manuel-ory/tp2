module.exports = function(grunt) {
  grunt.initConfig({
	  pkg: grunt.file.readJSON('package.json'),
	  concat: {
				options: {
					separator: "\n", //add a new line after each file
					banner: "", //added before everything
					footer: "" //added after everything
				},
				dist: {
					// the files to concatenate
					src: [
						'js/*.js',	
					],
					// the location of the resulting JS file
					dest: 'dist/<%= pkg.name %>.js'
				}
		},



	  uglify: {
	    	
	    	options: {
	      		banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
	    	},
	    	
	    	build: {
	      		src: 'dist/<%= pkg.name %>.js',
	      		dest: 'dist/<%= pkg.name %>.min.js'
	    	},
	  
	  },

	  watch: {
     
	      js: {
	        files: ['js/*.js', 'tests/*.js'],
	        tasks: ['concat','uglify','qunit'],
	        options: {
	        	livereload: true,
	        },

	      },

      },
     
      qunit: {
    	  all: ['html/*.html']
  	  }

  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-qunit');

  grunt.registerTask('build', ['concat', 'uglify', 'qunit']);
};